%include "lib.inc"
%define OFFSET 8

section .text


global find_word
find_word:
push r12
push r13
push rbx

mov r12, rdi
.iterator:
mov rdi, r12
mov rbx, rsi
add rsi, OFFSET
call string_equals
test rax, rax
jne .word_found

mov r13, [rbx];
test r13, r13
je .not_found

mov rsi, r13
jmp .iterator

.not_found:
xor rax, rax
jmp .end

.word_found:
mov rax, rbx

.end:
pop rbx
pop r13
pop r12
ret
