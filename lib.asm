
%define WRITE_SYSCALL 1
%define STDIN 0
%define STDERR 2


; Принимает код возврата и завершает текущий процесс
global exit
exit:
mov rax, 60
syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
global string_length
string_length:
mov rax, -1
.while:
inc rax
cmp byte [rdi+rax], 0
jne .while
ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
global print_string
print_string:
push rdi
call string_length
pop rdi

mov rdx, rax
mov rax, 1
mov rsi, rdi
mov rdi, 1
syscall
ret


; Принимает код символа и выводит его в stdout
global print_char
print_char:
push rdi

mov rax, 1
mov rsi, rsp
mov rdi, 1
mov rdx, 1
syscall

pop rdi
ret

; Переводит строку (выводит символ с кодом 0xA)
global print_newline
print_newline:
mov rdi, 10
jmp print_char


; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
global print_uint
print_uint:
mov rcx, 10
mov r8, rsp
mov rax, rdi
xor rdx, rdx
push 0

.loop:
div rcx
add dl, '0'
dec rsp
mov r9b, byte [rsp]
mov byte [rsp], dl
xor rdx, rdx
test rax, rax
jnz .loop

mov rdi, rsp
push r8
call print_string
pop rsp
ret


; Выводит знаковое 8-байтовое число в десятичном формате
global print_int
print_int:
test rdi, rdi
jnl .positive
neg rdi
push rdi
mov rdi, '-'
call print_char
pop rdi
.positive:
jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
global string_equals
string_equals:
mov rax, 1
.while:
mov r8b, byte[rdi]
mov r9b, byte[rsi]
inc rdi
inc rsi

cmp r8b, r9b
jne .wrong

test r8b, r8b
je .return

jmp .while
.wrong:
xor rax, rax
.return:
ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
global read_char
read_char:
xor rax, rax
xor rdi, rdi
dec rsp
mov rsi, rsp
mov rdx, 1
syscall

mov r9b, byte[rsp]
test rax, rax
jz .func

mov al, r9b
inc rsp
ret

.func:
xor r9b, r9b
mov al, r9b
inc rsp
ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
global read_word
read_word:
push r13
push r15
push r14

test rsi, rsi
jz .success_in_start

mov r15, rdi
mov r14, rsi

.while:
cmp r14, 1
jbe .wrong
call read_char

cmp al, ' '
je .checker_null

cmp al, `\t`
je .checker_null

cmp al, `\n`
je .checker_null

test al, al
jz .success

cmp r13, r14
jnb .wrong
mov [r15 + r13], al
inc r13
jmp .while

.checker_null:
test r13, r13
je .while

.success:
mov byte[r13+r15], 0x0

.success_in_start:
mov rax, r15
mov rdx, r13
jmp .return

.wrong:
xor rax, rax

.return:
pop r14
pop r15
pop r13
ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
global parse_uint
parse_uint:
xor rdx, rdx
xor rax, rax
xor r11, r11
mov r10, 10
_A:
mov r9b, byte [rdi]
cmp r9b, '0'
jb _E
cmp r9b, '9'
ja _E
sub r9b, '0'

mul r10
add al, r9b
inc r11
inc rdi
jmp _A
_E:
mov rdx, r11
ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
global parse_int
parse_int:
mov r11b, byte[rdi]
cmp r11b, '-'
jnz .positive
cmp r11b, '+'
jz .positive

add rdi, 1
push r11
call parse_uint
pop r11
neg rax
inc rdx
jmp .exit
.positive:
call parse_uint
.exit:
ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
global string_copy
string_copy:
xor rax, rax

.while:
cmp rax, rdx
jg .overflow

mov r11b, byte [rdi+rax]
mov byte [rsi+rax], r11b
test r11b, r11b
jz .return
inc rax

jmp .while
.overflow:
xor rax, rax
.return:
ret

global read_stdin
read_stdin:
mov rdx, rsi
mov rsi, rdi
mov rdi, STDIN
xor rax, rax
syscall
ret

global print_error
print_error:
push rdi
call string_length
pop rdi
mov rdx, rax
mov rax, 1
mov rsi, rdi
mov rdi, 2
syscall
ret

global read_line
push r12
push r13
push r14
xor r14, r14        
mov r12, rdi        
mov r13, rsi       
.buf:
test r13, r13    
jz .stop
.symbols:
call read_char    
test rax, rax         
jz .stop
cmp al, `\n`       
je .stop
mov byte[r12+r14], al   
inc r14           
cmp r14, r13        
 jae .long
jmp .symbols       
.long:
xor rax, rax       
jmp .ret
.stop:    
mov rdx, r14        
mov rax, r12        
.ret:
pop r14
pop r13
pop r12
ret

