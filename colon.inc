%define data_top 0
%macro colon 2
    %ifid %2
        %2: dq data_top
        %define data_top %2
    %else
        %error "Invalid colon label: %2"
    %endif
    %ifstr %1
        db %1, 0
    %else
        %error "Invalid colon string: %1"
    %endif
%endmacro

