from subprocess import Popen, PIPE

test_data = [
    ("fruit1", "", "Not found", ""),
    ("12345", "", "Not found", ""),
    ("apple", "Red", "", ""),
    ("!@#$%^", "", "Not found", ""),
    ("orange", "Orange", "", ""),
    ("book", "", "Not found", ""),  
    ("grape", "Purple", "", ""),
    ("", "", "Not found", ""),
    ("banana", "Yellow", "", ""),
    ("abcdef", "", "Not found", ""),
    ("987654321", "", "Not found", ""),
    ("grapefruit", "", "Not found", ""),  
    ("wrfgbewthnwrsthnwrnrghnrnjrgn", "", "Not found", ""),
    ("wrfgbewthnwrsthnwrnrghnrnjrgnawlkbqhrejgaqneqarvjqqqqqqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjqjq", "", "Failed to read word", ""),

]

def run_tests():
    for i, (input_data, expected_output, expected_error, _) in enumerate(test_data):
        process = Popen(["./main"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate(input=input_data.encode())
        stdout = stdout.decode().strip()
        stderr = stderr.decode().strip()


        result = "PASSED" if stdout == expected_output and stderr == expected_error else "FAILED"

        print(f"Test {i + 1}: {result}")
        print(f"Input: {input_data}")
        print(f"Expected output: {expected_output}")
        print(f"Actual output: {stdout}")
        print(f"Expected error: {expected_error}")
        print(f"Actual error: {stderr}")
        print(f"Exit code: {process.returncode}")
        print("-------------------")

run_tests()

