%include "dict.inc"
%include "lib.inc"
%include "words.inc"

%define INDEX_OFFSET 8
%define NEW_LINE_CHAR 0xA
%define BUFFER_SIZE 255

global _start

section .rodata
not_found_message: db "Not found", 0
read_fail_message: db "Failed to read word", 0

section .bss
input_buffer: resb BUFFER_SIZE

section .text

_start:
mov rdi, input_buffer
mov rsi, BUFFER_SIZE
call read_line
test rax, rax
jz .read_input_failed

push rdx
mov rdi, input_buffer
mov rsi, data_top
call find_word
pop rdx

test rax, rax
jz .word_not_found

mov rdi, rax
add rdi, INDEX_OFFSET
add rdi, rdx
inc rdi
call print_string
jmp exit

.word_not_found:
mov rdi, not_found_message
jmp .error_out

.read_input_failed:
mov rdi, read_fail_message

.error_out:
call print_error
